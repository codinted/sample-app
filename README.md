### Node sample app 
This is simple node express app created with express generator on gitlab git repository.


Run ``` npm install``` to install the node modules.

Run ``` npm run test``` to test the node app.
Run ``` npm run start``` to start the node app on localhost:3000.

To start the app in debug mode:
``` DEBUG=helloworld:* npm start```





### Docker

Run ``` docker build -t node/sample-app .```  in the project root to build image.
Run ``` docker run -p 3000:3000 -p 5000:5000 -e NODE_ENV="development" node/sample-app``` to run the node app with node env development.


You can access the the app on 

### CI/CD with Jenkins

Jenkinsfile is added in the project root which describes the multibranch pipeline.
Pipeline is extended with shared-library. (https://gitlab.com/codinted/shared-library.git)
The reusable pipeline is described in vars/node_basic.groovy in abouv git repo.

The pipeline can be imported global pipeline library with with name and default version which can be attributed to branch or commit_id and git repo details.

We call this library in our jenkins file and provide the project specific variable.
In this case we can provide docker image name with config.image_name to build and groovy closure in which we can add new stage or step.

Pipeline is using docker container as agent. There are three stages to pipeline.
Last one is build relevant to the branch.

The multi branch pipeline is configured for development and production branch in deploy stages.

Deploy stage can be further elaborated with push to docker registry, deploy to kubernetes, run ansible-playbook which will deploy to kubernetes.



